#include<iostream>
#include<cstdlib>
#include<vector>
#include<ctime>
#include<algorithm>
#include"bank.h"

using namespace std;

void poomSort(vector<customer> &);

void main(){
	srand(time(0));
	vector<customer> queue;
	int customerNum , maxCust, minCust, maxService, minService,index,w8, cashierNo = 0;
	float totalWaitTime = 0 ;

	cout << "enter a number of cashier";
	cin >> cashierNo;
	cout << "enter min customer: ";
	cin >> minCust;
	cout << "enter max customer: ";
	cin >> maxCust;	
	cout << "enter min service time : ";
	cin >> minService;
	cout << "enter max service time : ";
	cin >> maxService;

	int *left = new int[cashierNo];
	for (int i = 0; i < cashierNo; i++) { //make all index to be 0
		left[i] = 0;
	}

	customerNum = (rand() % (maxCust - minCust)) + minCust; //generate customer number

	for (int i = 0; i < customerNum; i++) {
		queue.push_back(customer(minService, maxService)); //generate arrive time and service time for each customer
	}

	poomSort(queue); //sort customer queue by arrive time

	/*for (int i = 0; i < customerNum; i++) {
		cout <<"Cust " << i+1 << " " << queue[i].get_arr() << " " << queue[i].get_ser() << endl;
	}*/

	for (int i = 0; i < customerNum; i++) {
		index = 0;
		w8 = left[0] - queue[i].get_arr(); //wait time = left time - arrive time
		for (int j = 0; j < cashierNo; j++) {
			if (w8 <= 0) { //no queue in cashier
				break;
			}
			else {
				if (w8 > left[j] - queue[i].get_arr()) { //if customer have to wait
					index = j; //move to next cashier
					w8 = left[j] - queue[i].get_arr(); //wait time of fastest left cashier
				}
			}
		}
		if (w8 <= 0) {
			w8 = 0;
			left[index] = queue[i].get_arr() + queue[i].get_ser();	 //left time = arrive time + service time 
		}
		else {
			left[index] += queue[i].get_ser();
			totalWaitTime += w8; //collect total wait time to calculate avg wait time
		}

	
		
		cout << "customer:" << i + 1 << " --- Arrive at : " << queue[i].get_arr() <<" --- sevice time : "<< queue[i].get_ser() << " --- wait time : " << w8 <<" --- left at : "<< left[index] << endl;
	}

	cout << "avg wait time : " << totalWaitTime / customerNum;

	delete [] left;
	system("pause");
}


void poomSort(vector<customer> &cust) {
	for (int i = 0; i < cust.size(); i++) {
		for (int j = 0; j < cust.size(); j++) {
			if (cust[j].get_arr() > cust[i].get_arr()) {
				customer tmp;
				tmp = cust[j];
				cust[j] = cust[i];
				cust[i] = tmp;
			}
		}
	}
}